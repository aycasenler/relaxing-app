package com.example.relaxingapp


import android.app.AlertDialog
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.custom_dialog_design.view.*
import jp.wasabeef.picasso.transformations.CropCircleTransformation

class MainActivity : AppCompatActivity() {
    private var list: ListView? = null
    private var music: MediaPlayer? = null

    var stormImageUrl =
        "https://images.unsplash.com/photo-1504123010103-b1f3fe484a32?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
    var birdImageUrl =
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfcw_0lVEWbrm5cGT3VEzfqYUwwX0UObVL1MVCTARs230NCKxk"
    var forestImageUrl =
        "https://images.unsplash.com/photo-1448375240586-882707db888b?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjEyMDd9"
    var rainImageUrl =
        "https://media.mnn.com/assets/images/2017/03/raindrops-plants-smell.jpg.653x0_q80_crop-smart.jpg"
    var oceanImageUrl =
        "https://www.yummymummyclub.ca/sites/default/files/bigstock-Powerful-Blue-Ocean-Wave-17176880.jpg"
    var windImageUrl =
        "https://wgbh.brightspotcdn.com/dims4/default/766148c/2147483647/strip/true/crop/2385x1301+2+131/resize/990x540!/quality/90/?url=https%3A%2F%2Fwgbh.brightspotcdn.com%2Fc6%2Feb%2F8931e8834060b84213f42435ba83%2Fblock-island-turbines.jpg"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        mlistView()
    }

    fun init() {
        list = findViewById(R.id.list_item)

        list?.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->

            val choosen = parent.getItemAtPosition(position) as String

            Toast.makeText(this, "$choosen is choosen ", Toast.LENGTH_SHORT).show()
            customDialog(position)
        }
    }

    private fun customDialog(position: Int) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_design, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        when (position) {
            0 -> {
                mDialogView.choosenText.text = list?.getItemAtPosition(position) as String
                picassoImage(mDialogView, birdImageUrl)
                music = MediaPlayer.create(this, R.raw.bird)
                music?.start()
            }

            1 -> {
                mDialogView.choosenText.text = list?.getItemAtPosition(position) as String
                //mDialogView.choosenText.setText(sounds[position])
                picassoImage(mDialogView, rainImageUrl)
                music = MediaPlayer.create(this, R.raw.rain)
                music?.start()
            }
            2 -> {
                mDialogView.choosenText.text = list?.getItemAtPosition(position) as String
                picassoImage(mDialogView, forestImageUrl)

                music = MediaPlayer.create(this, R.raw.forest)
                music?.start()
            }
            3 -> {
                mDialogView.choosenText.text = list?.getItemAtPosition(position) as String
                picassoImage(mDialogView, stormImageUrl)
                music = MediaPlayer.create(this, R.raw.storm)
                music?.start()
            }
            4 -> {
                mDialogView.choosenText.text = list?.getItemAtPosition(position) as String
                picassoImage(mDialogView, oceanImageUrl)
                music = MediaPlayer.create(this, R.raw.ocean)
                music?.start()
            }
            5 -> {
                mDialogView.choosenText.text = list?.getItemAtPosition(position) as String
                picassoImage(mDialogView, windImageUrl)
                music = MediaPlayer.create(this, R.raw.wind)
                music?.start()
            }
        }
        mDialogView.cancelBtn.setOnClickListener {

            mAlertDialog.dismiss()
            music?.stop()
        }
    }

    fun mlistView() {
        list?.adapter = CustomAdapter(this)
    }

    fun picassoImage(mDialogView: View, url: String) {
        // Picasso.get().load(url).into(mDialogView.choosenImage)
        Picasso.get().load(url).resize(600, 600).transform(CropCircleTransformation())
            .into(mDialogView.choosenImage)
    }
}






