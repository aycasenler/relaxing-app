package com.example.relaxingapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.list_design.view.*

class CustomAdaptor(private val context: Context) : BaseAdapter() {

     data class myList(val sounds: String, val url: String)
         val mylist1 = myList(
             "Bird Sound",
             "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfcw_0lVEWbrm5cGT3VEzfqYUwwX0UObVL1MVCTARs230NCKxk"
         )
         val mylist2 = myList(
             "Rain sound",
             "https://media.mnn.com/assets/images/2017/03/raindrops-plants-smell.jpg.653x0_q80_crop-smart.jpg"
         )
         val myList3 = myList(
             "Forest Sound",
             "https://images.unsplash.com/photo-1448375240586-882707db888b?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjEyMDd9"
         )
         val mylist4 = myList(
             "ThunderStorm",
             "https://images.unsplash.com/photo-1504123010103-b1f3fe484a32?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
         )
         val mylist5 = myList(
             "Ocean Sound",
             "https://www.yummymummyclub.ca/sites/default/files/bigstock-Powerful-Blue-Ocean-Wave-17176880.jpg"
         )
         val myList6 = myList(
             "Wind Sound",
             "https://wgbh.brightspotcdn.com/dims4/default/766148c/2147483647/strip/true/crop/2385x1301+2+131/resize/990x540!/quality/90/?url=https%3A%2F%2Fwgbh.brightspotcdn.com%2Fc6%2Feb%2F8931e8834060b84213f42435ba83%2Fblock-island-turbines.jpg"
         )

        val listOfObjects = listOf(mylist1, mylist2, myList3, mylist4, mylist5, myList6)


    val sounds = listOf(
        "Bird Sound",
        "Rain sound",
        "Forest Sound",
        "ThunderStorm",
        "Ocean Sound",
        "Wind Sound"
    )
    val urls = listOf(
        "https://images.unsplash.com/photo-1504123010103-b1f3fe484a32?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"
        ,
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfcw_0lVEWbrm5cGT3VEzfqYUwwX0UObVL1MVCTARs230NCKxk"
        ,
        "https://images.unsplash.com/photo-1448375240586-882707db888b?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjEyMDd9"
        ,
        "https://images.unsplash.com/photo-1448375240586-882707db888b?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjEyMDd9"
        ,
        "https://media.mnn.com/assets/images/2017/03/raindrops-plants-smell.jpg.653x0_q80_crop-smart.jpg"
        ,
        "https://media.mnn.com/assets/images/2017/03/raindrops-plants-smell.jpg.653x0_q80_crop-smart.jpg"
        ,
        "https://www.yummymummyclub.ca/sites/default/files/bigstock-Powerful-Blue-Ocean-Wave-17176880.jpg"
        ,
        "https://wgbh.brightspotcdn.com/dims4/default/766148c/2147483647/strip/true/crop/2385x1301+2+131/resize/990x540!/quality/90/?url=https%3A%2F%2Fwgbh.brightspotcdn.com%2Fc6%2Feb%2F8931e8834060b84213f42435ba83%2Fblock-island-turbines.jpg"

    )


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        //val erisim = listOfObjects(position)
        val layoutinflater = LayoutInflater.from(context)

        val listDesing = layoutinflater.inflate(R.layout.list_design, parent, false)

        val listTv = listDesing.findViewById<TextView>(R.id.listTv)

        listTv.text = listOfObjects.get(position).sounds
            //sounds.get(position)

        picassoImage(listDesing, listOfObjects.get(position).url)



        return listDesing

    }

    override fun getItem(position: Int): Any {
        return sounds[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return sounds.size
    }

    fun picassoImage(listDesing: View, url: String) {
        // Picasso.get().load(url).into(mDialogView.choosenImage)
        Picasso.get().load(url).resize(250, 250).transform(CropCircleTransformation())
            .into(listDesing.listImg)

    }

}